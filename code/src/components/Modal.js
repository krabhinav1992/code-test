import "./Modal.css";

function Modal({modalData,closeToggle }) {
console.log(modalData)
var renderdataObj = JSON.parse(modalData);
  return (
    <>
      <div className="modal">

<div className="modal-content">
    <div className="parent">
    <div>
    <img src={renderdataObj.row.avatar} width="100" />
    <p>{renderdataObj.row.jobTitle}</p>
    <p>{renderdataObj.row.contactNo}</p>
    </div>
  <div><b>{renderdataObj.row.firstName} {renderdataObj.row.lastName}</b>
  <p>{renderdataObj.row.bio}</p></div>
  </div>
  
  <button className="close-modal" onClick={() => {
              closeToggle(true);
            }}>
    X
  </button>
</div>
</div>
    </>
  );
}

export default Modal;
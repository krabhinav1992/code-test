import React ,{ useState } from 'react';

import Modal from './Modal';
const TableBody = ({ data, columns }) => {

  const [modal, setModal] = useState(false);
  const [dataJSON, setData] = useState();
 
const modalData = (row) => {
  setModal(!modal);
  setData(JSON.stringify({row}));
};
const closeToggle = (modal) => { // the callback.
  setModal(!modal);
};

  return (
    <tbody>
      {data.map((data) => {
        return (
          <tr key={data.id}>
            {columns.map(({ accessor }) => {
              const tData = data[accessor] ? data[accessor] : "——";
              return <td onClick={(event) => modalData(data)} key={accessor}>{tData}</td>;
            })}
          </tr>
        );
      })}
      {modal && (
           <Modal modalData={dataJSON} closeToggle={closeToggle}/>
      )}
    </tbody>
    
  );
};

export default TableBody;

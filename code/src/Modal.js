import "./Modal.css";

function Modal({modalData,closeToggle }) {

var renderdataObj = JSON.parse(modalData);
  return (
    <>
      <div className="modal">

<div className="modal-content">
  <h2>Hello Modal</h2>
  {renderdataObj.row.firstName}
  <button className="close-modal" onClick={() => {
              closeToggle(true);
            }}>
    X
  </button>
</div>
</div>
    </>
  );
}

export default Modal;
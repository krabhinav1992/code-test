import React ,{ useState } from 'react';
import Table from "./components/Table";
import companyInfo from "./sample-data.json";

const columns = [
  { label: "First Name", accessor: "firstName", sortable: true },
  { label: "Job Title", accessor: "jobTitle", sortable: false },
  { label: "Contact Number", accessor: "contactNo", sortable: true },
  { label: "Address", accessor: "address", sortable: true },
];
const dataJson = companyInfo.employees;

const App = () => {
  const [query,setQuery] = useState("");

function search(data) {
  
  return data.filter((item)=> item.firstName.includes(query))
  
}
console.log(search(dataJson));
  return (
    <div className="table_container">
      <h1>{companyInfo.companyInfo.companyName}</h1>
      <h3>{companyInfo.companyInfo.companyMotto}</h3>
      <input 
      type="text"
      className="inputContainer"
      onChange={(e) => setQuery(e.target.value)}
      />
      <Table
        data={search(dataJson)}
        columns={columns}
      />
    </div>
  );
};

export default App;
